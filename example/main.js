'use strict'
const electron = require('electron')
const app = electron.app
const path = require('path')
const { prepareDialogs, openDialog } = require('electron-custom-dialog')

// Prevent window being garbage collected
let mainWindow

function onClosed() {
  // Dereference the window
  // For multiple windows store them in an array
  mainWindow = null
}

function createMainWindow() {
  mainWindow = new electron.BrowserWindow({
    width: 600,
    height: 400
  })
  let isQuitDialogOpen = false
  // mainWindow.webContents.openDevTools()
  mainWindow.on('close', e => {
    e.preventDefault()
    if (!isQuitDialogOpen) {
      isQuitDialogOpen = true
      openDialog('simpleQuestion', { title: 'quit?', question: 'Do you want to quit?' }).then(
        yes => {
          if (yes) {
            mainWindow.removeAllListeners('close')
            app.quit()
          }
          isQuitDialogOpen = false
        }
      )
    }
  })

  prepareDialogs({
    name: 'simpleQuestion',
    parent: mainWindow,
    load: (win, props) => {
      // win.webContents.openDevTools()
      win.setTitle(props.title)
      win.loadFile(path.join(__dirname, 'dialog.html'))
    },
    windowOptions: {
      devTools: false,
      width: 400,
      height: 150
    }
  })

  mainWindow.loadFile(path.join(__dirname, 'renderer.html'))
  mainWindow.on('closed', onClosed)

  return mainWindow
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (!mainWindow) {
    createMainWindow()
  }
})

app.on('ready', () => {
  mainWindow = createMainWindow()
})
